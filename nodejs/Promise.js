const fs = require('fs');

fs.readFile('./run.bat', 'utf-8', (err, data) => {
    if (err) {
        // 处理错误
        console.error(err);
          return;
    }
    console.log(data);
});

for (let i = 0; i < 10; i++) {
    console.log(i);
}

const util = require('util');
const readFile2 = util.promisify(fs.readFile);

readFile2('./run.bat', 'utf-8')
    .then(data => {
        console.log(data);
    })
    .catch(err => {
        console.log(err);
    });


(async () => {
        try {
            const content = await readFile2('./run.bat', 'utf-8');
            console.log(content);
        } catch (err) {
            console.error(err);
        }
    })();

const readFile3 = (fileName, encoding) => {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, encoding, (err, data) => {
            if (err) {
                return reject(err);
            }

            resolve(data);
        });
    });
}

readFile3('./run.bat','utf-8')
    .then(data => {
        console.log(data);
    })
    .catch(err => {
        console.log(err);
    });