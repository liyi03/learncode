// print
console.log("Hello World");

// var

var var_x="hello"
var_y="world"

// function

function say(word){
    console.log(word);
}

function execute(someFunction, value){
    someFunction(value);
}

execute(say,var_x);
execute(say,var_y);

module.exports=say;   //模块的输出暴露给其他模块。用var ref = require('module_name');就拿到了引用模块的变量。

//require , module.exports 是CommonJS/AMD规范引入方式
// import export 是是ES6的一个语法标准