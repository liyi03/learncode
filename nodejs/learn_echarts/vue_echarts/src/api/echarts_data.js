import request from "../utils/request";

export function getAppreopsize(data){
    return request({
        url:'/scm/reposize/',
        method: 'get',
        params: data
    })
}

export function getAppscmCSD(data){
    return request({
        url:'/scm/csu/',
        method: 'get',
        params: data
    })
}


export function getUser(data){   // 测试mook用
    return request({
        url:'/users',
        method: 'get',
        params: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
}