// import Vue from 'vue';

const app = new Vue({
    el: '#app',
    data: {
      message: 'Hello Vue!',
      iff:false
    },
    created: function () {
        console.log('a is: ' + this.message)
      },
    computed: {
        // 计算属性的 getter
        reversedMessage: function () {
          // `this` 指向 vm 实例
          return this.message.split('').reverse().join('')
        }
      }
  })


app.message="1234"

console.log(app.message)
console.log(app.reversedMessage)
console.log(typeof app.reversedMessage)


// Vue.component('todo-item', {
//     template: '<li>这是个待办项</li>'
//   })