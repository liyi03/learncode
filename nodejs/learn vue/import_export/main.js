
import detail from './module1';  //不需要加花括号  只能一个一个导入

console.log(detail('Said', 20)); // Hello Said, your age is 20 year old!

//需要加花括号  可以一次导入多个也可以一次导入一个，但都要加括号

import {detail, userProfile, getPosts} from './module1';

console.log(detail('Said', 20)); 
console.log(userProfile); 
console.log(getPosts);


// 如果是两个方法

import {axiosfetch,post}  from './module1'


// 第二种  导入成组的方法

import * as tools from './libs/tools'

// 其中tools.js中有多个export方法,把tools里所有export的方法导入

// vue中怎么用呢？
Vue.prototype.$tools = tools
// 直接用 this.$tools.method调用就可以了