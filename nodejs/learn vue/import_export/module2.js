var var_x="hello"
var_y="world"

// function

function say(word){
    console.log(word);
}

function execute(someFunction, value){
    someFunction(value);
}

execute(say,var_x);
execute(say,var_y);

export default say;

// module.exports=say;   //模块的输出暴露给其他模块。用var ref = require('module_name');就拿到了引用模块的变量。
// 再是 export default 
