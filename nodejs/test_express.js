var express=require('express');
var app=express();


app.get(
    '/',
    function(request,response){
        response.send('Hello World!');
    }
);


app.get(
    '/about',
    function(request,response){
        response.send('Welcome to the about page!');
    }
);

app.get(
    "*",
    function(request,response){
        response.send("404 error!");
    }
);

app.all(
    "*",
    function(request,response,next){
        response.writeHead(200,{"Content-Type":"text/html;charset=utf-8"}); //设置响应头属性值
        next();
    }
);



app.listen(8011);