package org.example.demo1explore.app1.controller;


import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.example.demo1explore.app1.Vo.RequsetInVo1;
import org.example.demo1explore.app1.Vo.ResponseOutVo;
import org.example.demo1explore.app1.service.Service1;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/app1")
@Validated
public class Controller1 {

    @Resource
    private Service1 service1;

    @GetMapping(value = "/hello")
    public String hello() {
        Assert.isTrue(false, "errot false");
        return "Hello World!";
    }

    @PostMapping(value = "/hello")
    public ResponseOutVo hello(@Valid @RequestBody RequsetInVo1 vo1) {
        System.out.println(vo1);
        return new ResponseOutVo("hello", "200", vo1);
    }

    @GetMapping(value = "/hello2")
    public String hello1() {
        return service1.hello();
    }

    @ExceptionHandler
    public ResponseOutVo handleException(IllegalArgumentException e) {
        return new ResponseOutVo("error", "error", e.getMessage());
    }

    @ExceptionHandler
    public ResponseOutVo handleException(MethodArgumentNotValidException e) {
        System.out.println(e.getBindingResult().getFieldErrors().stream().map(fieldError -> fieldError.getDefaultMessage()).collect(Collectors.toList()));

        return new ResponseOutVo("error", "error", e.getBindingResult().getFieldErrors().stream().map(fieldError -> fieldError.getDefaultMessage()).collect(Collectors.toList()));

    }

}
