package org.example.demo1explore.app1.Vo;


public class ResponseOutVo {
    private String code;
    private String msg;
    private Object data;

    public <R> ResponseOutVo(String error, String error1, R collect) {
        this.code = error;
        this.msg = error1;
        this.data = collect;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
