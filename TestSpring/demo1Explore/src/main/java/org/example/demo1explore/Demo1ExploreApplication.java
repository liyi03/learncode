package org.example.demo1explore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Demo1ExploreApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(Demo1ExploreApplication.class);

        //        springApplication.run(args);
        ConfigurableApplicationContext run = springApplication.run(args);

        // 获取容器中所有bean的名称
        String[] beanDefinitionNames = run.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }
        System.out.println(beanDefinitionNames.length);
    }
}
