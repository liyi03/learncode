package org.example.demo1explore.app1.Vo;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;

import java.io.Serializable;

/**
 * @author liyi
 * 必须有getter setter,不然请求参数写不到这个对象里面。
 */
public class RequsetInVo1 {

    @NotBlank( message= "name is required")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @NotBlank( message= "age is required")
    private String age;

    @Override
    public String toString() {
        return this.name+" "+this.age;
    }
}
