package basic;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class testThread {

    // 创建并启动新线程，方法1：继承Thread类
    public static void func1() {
        MyThread thread = new MyThread();
        thread.start();
    }

    // 创建并启动新线程，方法2：实现Runnable接口 （无法直接获取线程返回值）
    public static void func2() {
        Thread thread2 = new Thread(new MyRunnable());
        thread2.start();
    }

    // 创建并启动新线程，方法3：实现Callable接口
    public static void func3() {
        // 创建一个 ExecutorService 实例
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        // 提交 Callable 任务
        Future<Integer> future = executorService.submit(new MyCallable());

        try {
            // 等待任务执行完毕并获取返回值
            System.out.println("Future is done? " + future.isDone());
            int result = future.get();  //此处会阻塞主线程，直到任务执行完毕
            System.out.println("Result: " + result);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 关闭 ExecutorService
        executorService.shutdown();

        // 多个任务同时执行
        // 创建一个固定大小的线程池
        ExecutorService executorService2 = Executors.newFixedThreadPool(5);

        // 创建一个 Callable 集合
        List<Callable<Integer>> callableList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            callableList.add(new MyCallable());
        }

        try {
            // 并行执行 Callable 任务
            List<Future<Integer>> futures = executorService2.invokeAll(callableList);

            // 遍历获取每个任务的返回值并打印
            for (Future<Integer> futureI : futures) {
                Integer result = futureI.get();
                System.out.println("Result: " + result);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // 关闭线程池
        executorService2.shutdown();
    }


    // 创建并启动新线程，方法4: 使用线程池
    public static void func4() {
        // 创建一个固定大小的线程池，大小为 2
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        // 提交多个任务给线程池执行
        for (int i = 0; i < 5; i++) {
            executorService.execute(new MyRunnable());
        }

        // 关闭线程池
        executorService.shutdown();
    }

    public static void main(String[] args) {
        func1();
        func2();
        func3();
        func4();
    }
}

// java创建线程方法1：继承Thread类
class MyThread extends Thread {
    @Override
    public void run() {
        // 线程要执行的任务
        for (int i = 0; i < 500; i++) {
            System.out.println("Thread running: " + i);
            try {
                Thread.sleep(1000); // 暂停 1 秒
            } catch (InterruptedException e) {
                System.out.println("Thread interrupted.");
            }
        }
    }
}

// java创建线程方法2：实现Runnable接口 （无法直接获取线程返回值）
class MyRunnable implements Runnable {
    @Override
    public void run() {
        // 线程要执行的任务
        for (int i = 0; i < 5; i++) {
            System.out.println("Thread " + Thread.currentThread().getName() + ", running: " + i);
            try {
                Thread.sleep(1000); // 暂停 1 秒
            } catch (InterruptedException e) {
                System.out.println("Thread interrupted.");
            }
        }
    }
}

// java创建线程方法3：实现Callable接口
class MyCallable implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        // 线程要执行的任务
        for (int i = 0; i < 5; i++) {
            System.out.println("Thread " + Thread.currentThread().getName() + ", running: " + i);
            try {
                Thread.sleep(1000); // 暂停 1 秒
            } catch (InterruptedException e) {
                System.out.println("Thread interrupted.");
            }
        }
        return 3;
    }
}