package basic;

import java.util.concurrent.locks.ReentrantLock;

public class testReentrantLock implements Runnable {
    private final ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        testReentrantLock example = new testReentrantLock();

        // 创建并启动两个线程
        Thread thread1 = new Thread(example);
        Thread thread2 = new Thread(example);
        thread1.start();
        thread2.start();
    }

    @Override
    public void run() {
        // lock.lock();          //            获取不到-等待。
        if (!lock.tryLock()) {   // ⭐尝试获取锁，获取不到-跳过
            System.out.println("获取锁失败");
            return;
        }
        try {            // 执行需要同步的代码块
            System.out.println("执行同步代码块");
            nestedMethod(); // 在同步代码块中调用另一个需要同步的方法 (⭐可重入)
        } finally {
            System.out.println("释放锁-exampleMethod");
            lock.unlock(); // 释放锁
        }
    }

    public void nestedMethod() {
        lock.lock();
        try {               // 执行需要同步的代码块
            System.out.println("执行嵌套的同步方法");
        } finally {
            lock.unlock(); // 释放锁
        }
    }
}
