package basic;
import java.util.ArrayList;
import java.util.Arrays;

class TestSubList {
    public static void main(String[] args){

        // [1][ArrayList]创建一个动态数组
        ArrayList<String> sites = new ArrayList<>();

        sites.add("0-Google");
        sites.add("1-Runoob");
        sites.add("2-Taobao");
        sites.add("3-Wiki");
        System.out.println("网站列表: " + sites);

        // 元素位置为1到3
        System.out.println("SubList: " + sites.subList(1, 3));

        sites.subList(1, 3).clear();
        System.out.println("删除1、2后的列表: " + sites);


        // 转换为 String[]
        String[] array = sites.toArray(new String[0]);
        System.out.println("String[] array="+Arrays.toString(array));

        // substring 分割
        String ssss ="V1.2.3.4.555555";
        System.out.println(ssss.substring(0,ssss.lastIndexOf('.')));

        String ssss2 ="V1234555.555";
        if (ssss2.lastIndexOf('.')>0){
            System.out.println(ssss2.substring(0,ssss2.lastIndexOf('.')));
        }
    }
}
