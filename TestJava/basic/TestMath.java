
public class TestMath {
    public static void main(String[] args) {
        float s = 0.75f;
        int cpuNum = 2;
        System.out.println("计算round: " + Math.round(cpuNum * s));
        System.out.println("计算线程数: " + getThreadNum(cpuNum, s));
    }

    /**
     * 计算线程数方法
     * 
     * @param cpuNum cpu核心数
     * @param s      系数
     * @return 线程数
     */
    public static int getThreadNum(int cpuNum, float s) {
        return Math.max((int) Math.round(cpuNum * s), 1);
    }
}
