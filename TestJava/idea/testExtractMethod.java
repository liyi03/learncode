package TestJava.idea;


import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * 测试idea代码提取为方法，
 * 快捷键ctrl+alt+m
 */
public class testExtractMethod {
    private static void extracted(LocalDateTime now) {
        long timestamp = now.toEpochSecond(ZoneOffset.of("+8"));
        long instant = (timestamp / 10) * 10;
        System.out.println(instant);
        System.out.println(LocalDateTime.ofEpochSecond(instant, 1, ZoneOffset.of("+8")).getSecond());
        System.out.println(LocalDateTime.ofEpochSecond(instant, 0, ZoneOffset.of("+8")).getSecond());
        DateTimeFormatter dfDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        System.out.println(dfDateTime.format(now));
        System.out.println(now.getSecond());
    }

    private void method1() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        System.out.println(now.toEpochSecond(ZoneOffset.of("+8")));

        extracted(now);
    }
}
