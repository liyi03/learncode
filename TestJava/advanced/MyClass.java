package advanced;

@MyAnnotation("== This is a sample class ==")
public class MyClass {
    // 类的成员和方法
    private int number;

    public MyClass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
