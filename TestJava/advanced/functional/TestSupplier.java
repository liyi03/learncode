package advanced.functional;

import java.util.function.Supplier;

public class TestSupplier {

    // 创建一个 Supplier 对象，返回一个随机数, 匿名类
    static Supplier<Double> randomSupplier = new Supplier<Double>() {
        @Override
        public Double get() {
            return Math.random();
        }
    };

    // 创建一个 Supplier 对象，返回一个随机数, lambda 表达式
    static Supplier<Double> randomSupplier2 = () -> Math.random();

    // 创建一个 Supplier 对象，返回一个随机数, 引用
    static Supplier<Double> randomSupplier3 = Math::random;

    public static void main(String[] args) {
        System.out.println(randomSupplier.get());  // 输出: 随机数，例如 0.723456789
        System.out.println(randomSupplier2.get());  // 输出: 随机数，例如 0.723456789
        System.out.println(randomSupplier3.get());  // 输出: 随机数，例如 0.723456789

    }


}
