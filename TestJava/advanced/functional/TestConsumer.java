package advanced.functional;

import java.util.function.Consumer;


/**
 * 函数式编程.
 * 测试Consumer接口
 */
public class TestConsumer {

    // 【demo1】消费一个String入参的方法
    private static void consumerString(Consumer<String> function) {
        function.accept("Hello");
    }

    private static void demo1() {
        consumerString(System.out::println);
    }

    // 【demo2】消费2个String入参的方法, 并不是先处理再处理。
    private static void consumerString(Consumer<String> func1, Consumer<String> func2, String str) {
        func1.andThen(func2).accept(str);
    }

    private static void demo2() {
        consumerString(String::toUpperCase, System.out::println, "demo2 string");
    }

    // 【main】
    public static void main(String[] args) {
        demo1();
        demo2();
    }
}
