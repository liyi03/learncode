package advanced.functional;

import java.util.function.Function;

public class TestFunction {

    // [demo1] 测试 Function
    private static String executeString(Function<String, String> func1){
        return func1.apply("hello");
    }

    private static void demo1(){
        String s1 = executeString(String::toUpperCase);
        System.out.println(s1);
    }

    // [demo2]
    private static void executeStrAndThen(Function<String, String> func1, Function<String, String> func2, String s){
        func1.andThen(func2).apply(s);
    }

    private static void demo2(){
        executeStrAndThen(String::toUpperCase, s->{
            System.out.println(s); return null;
        }, "hello2");
    }

    public static void main(String[] args)
    {
        demo1();
        demo2();
    }
}
