package advanced;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * https://www.liaoxuefeng.com/wiki/1252599548343744/1306581251653666
 * 实际上，可以把ThreadLocal看成一个全局Map<Thread, Object>：每个线程获取ThreadLocal变量时，总是使用Thread自身作为key：
 *
 * Object threadLocalValue = threadLocalMap.get(Thread.currentThread());
 * 因此，ThreadLocal相当于给每个线程都开辟了一个独立的存储空间，各个线程的ThreadLocal关联的实例互不干扰。
 *
 * 最后，特别注意ThreadLocal一定要在finally中清除：
 *
 * try {
 *     threadLocalUser.set(user);
 *     ...
 * } finally {
 *     threadLocalUser.remove();
 * }
 * 这是因为当前线程执行完相关代码后，很可能会被重新放入线程池中，如果ThreadLocal没有被清除，该线程执行其他代码时，会把上一次的状态带进去。
 */
public class testThreadLocal {

    public static void main(String[] args) {

            Thread thread2 = new Thread(new advanced.MyRunnable());
            thread2.start();
        Thread thread3 = new Thread(new advanced.MyRunnable());
        thread3.start();

    }


}

class MyRunnable implements Runnable {
    @Override
    public void run() {
        LocalUtil.add("Thread " + Thread.currentThread().getName());
        // 线程要执行的任务
        for (int i = 0; i < 5; i++) {
            System.out.println("LocalUtil.get() :"+LocalUtil.get() + ", running: " + i);
            try {
                Thread.sleep(1000); // 暂停 1 秒
            } catch (InterruptedException e) {
                System.out.println("Thread interrupted.");
            }
        }
        LocalUtil.remove();
    }
}

class LocalUtil {
    private static final ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void add(String value) {
        threadLocal.set(value);
    }

    public static void remove() {
        threadLocal.remove();
    }

    public static void set(String value) {
        threadLocal.set(value);
    }

    public static String get() {
        return threadLocal.get();
    }

}