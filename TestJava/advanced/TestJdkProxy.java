package advanced;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class TestJdkProxy {
    public static void main(String[] args) {

        // 1、创建IHello接口的代理, IHello接口无实现类
        IHello helloProxy = (IHello) Proxy.newProxyInstance(
                IHello.class.getClassLoader(),
                // IHello.class.getInterfaces(),
                // 如果此 Class 对象表示不实现接口的类或接口，则该方法返回长度为 0 的数组。运行会报错
                // 因此 Foo.class.getInterfaces() 实际上返回一个空数组。 得用下面的。
                // https://stackoverflow.com/questions/77339947/wierd-class-cast-using-dynamic-proxy-and-java17-java-modules-exception?newreg=42279a20a7a245efb3a316fa0b8708ea
                new Class<?>[]{IHello.class},
                new JDKProxy(null));
        helloProxy.sayHello();

        // 2、创建Hello对象的代理, IHello接口有实现类
        Hello hello = new Hello();
        IHello helloProxy2 = (IHello) Proxy.newProxyInstance(
                hello.getClass().getClassLoader(),
                hello.getClass().getInterfaces(),   // 实际obj获取
                new JDKProxy(hello));
        helloProxy2.sayHello();
    }
}

class Hello implements IHello {
    @Override
    public void sayHello() { System.out.println("say hello, by implement object");}
}

// 动态代理处理实现类
class JDKProxy implements InvocationHandler {
    private final Object target;
    public JDKProxy(Object target) { this.target = target;}

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("---------- 在业务方法调用之前可以进行前置增强   ------------");
        Object res = null;

        //利用反射机制调用方法，invoke为返回值，如果没有返回null
        if (target != null) {
            Object invoke = method.invoke(target, args);
            res = invoke;
        } else {
            System.out.println("say hello, by proxy, no implement object");
        }

        System.out.println("---------- 在业务方法调用之前可以进行后置增强   ------------");
        return res;
    }
}