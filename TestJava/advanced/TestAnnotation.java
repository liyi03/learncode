package advanced;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;


/**
 * @author liyi
 * 注解的原理，利用反射机制获取注解
 */
public class TestAnnotation {

    public static void main(String[] args) throws Exception {
        // 获取类上的注解
        Annotation[] annotations = advanced.MyClass.class.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println("Annotation: " + annotation.annotationType().getSimpleName());
            if (annotation instanceof MyAnnotation) {
                MyAnnotation myAnnotation = (MyAnnotation) annotation;
                System.out.println("Class Annotation: " + myAnnotation.value());
            }
        }

        // 获取字段上的注解
        Field field = MyClass.class.getDeclaredField("number");
        Annotation[] fieldAnnotations = field.getAnnotations();
        for (Annotation annotation : fieldAnnotations) {
            System.out.println("Field Annotation: " + annotation.annotationType().getSimpleName());
        }
    }
}