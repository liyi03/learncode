package advanced;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy; /**
 * @author liyi
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
    String value();
}
