package advanced;

// 被代理接口（jdk代理只能代理接口）
public interface IHello {
    void sayHello();
}
