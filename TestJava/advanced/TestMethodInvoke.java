package advanced;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

// 测试反射调用方法（method.invoke）
// 反射可以在不知类名，方法名，参数类型，参数个数，参数顺序的情况下，调用方法
// 反射可以在没有符号依赖的情况下，调用方法（例子中没有引入MyClass的符号依赖）
public class TestMethodInvoke {
    public static void main(String[] args) throws Exception {
        Class<?> clz = Class.forName("advanced.MyClass");

        // 通过反射调用构造方法，创建对象
        Constructor<?> constructor = clz.getConstructor(int.class);
        Object o = constructor.newInstance(10);

//      Method m = clz.getMethod("getNumber", String.class,Integer.class);
        Method m = clz.getMethod("getNumber");
        for (int i = 0; i < 16; i++) {
            Object invoked = m.invoke(o);
            System.out.println((int) invoked);
        }
    }
}
