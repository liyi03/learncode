#%%
import socket

with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as s:
    s.bind(("0.0.0.0",1234))
    s.listen()
    conn, addr = s.accept()

    with conn:
        while True:
            data = conn.recv(1024)
            if data:
                print(f"conn: {conn}, addr: {addr}, data: {data}")
                conn.sendall(b"get")
# %%
